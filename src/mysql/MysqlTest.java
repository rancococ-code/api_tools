package mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2016年11月2日 下午1:47:19
 * 
 */
public class MysqlTest {
	public static void main(String[] args) {
		try {
			Properties pro = new Properties();
			//根据机器配置，设置不同的参数
			pro.load(MysqlTest.class.getResourceAsStream("MySql_medium.properties"));
			new EmbedMySqlServer(pro).startup();
			//可以把数据库放到其他磁盘
			//new EmbedMySqlServer(pro,"f:\\").startup();
			Connection conn = getTestConnection();
			System.out.println(conn.isClosed());
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 测试数据库连接
	 */
	private static Connection getTestConnection() throws Exception {
		Connection conn = null;
		String driverName = "";
		try {
			String url = "";
			driverName = "org.gjt.mm.mysql.Driver";
			url = "jdbc:mysql://127.0.0.1:3336/mysql?useServerPrepStmts=false&useUnicode=true&characterEncoding=UTF-8";
			Class.forName(driverName);
			conn = DriverManager.getConnection(url, "root", "");
		} catch (final ClassNotFoundException e) {
			throw new Exception("没有找到数据库驱动类[" + driverName + "]", e);
		} catch (final SQLException e) {
			throw new Exception("数据库连接过程发生错误，请检查配置是否正确。", e);
		}
		return conn;
	}
}
