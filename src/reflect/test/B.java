package reflect.test;

/**
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2016年11月2日 下午3:34:59
 * 
 */
public class B implements IA {

	@Override
	public void print() {
		System.out.println(this.getClass().getCanonicalName());
	}

}
